package com.mifos.apache.fineract.utils;

/**
 * @author Rajan Maurya
 *         On 27/06/17.
 */
public class ConstantKeys {

    public static final String CUSTOMER_IDENTIFIER  = "customer_identifier";
}
